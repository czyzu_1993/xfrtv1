package mealsAndDrinks;


public class Drinks extends MealsAndDrinks {
    boolean iceCubes;
    boolean lemon;

    public Drinks(double price, String name) {
        super(price, name);
        iceCubes = false;
        lemon = false;
    }

    public Drinks(Drinks drinks) {
        super(drinks);
        iceCubes = drinks.iceCubes;
        lemon = drinks.lemon;
    }

    public void setIceCubes(boolean iceCubes) {
        this.iceCubes = iceCubes;
    }

    public void setLemon(boolean lemon) {
        this.lemon = lemon;
    }

}
