package mealsAndDrinks;


public abstract class MealsAndDrinks {
    private String name;
    private double price;

    public MealsAndDrinks(double price, String name) {
        this.price = price;
        this.name = name;
    }

    public MealsAndDrinks(MealsAndDrinks mealsAndDrinks) {
        this.price = mealsAndDrinks.price;
        this.name = mealsAndDrinks.name;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

}
