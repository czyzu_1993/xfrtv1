package mealsAndDrinks;

import enumeration.Cuisines;

public class Meals extends MealsAndDrinks {

    private Cuisines cuisies;

    public Cuisines getCuisies() {
        return cuisies;
    }

    public Meals(double price, String name, Cuisines cuisies) {
        super(price, name);
        this.cuisies = cuisies;

    }


}
