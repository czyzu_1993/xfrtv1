package orderAndCustomer;

import mealsAndDrinks.MealsAndDrinks;


public class SingleOrder {
    private int count;
    private MealsAndDrinks mealsAndDrinks;

    public SingleOrder(int count, MealsAndDrinks mealsAndDrinks) {
        this.count = count;
        this.mealsAndDrinks = mealsAndDrinks;
    }

    public int getCount() {
        return count;
    }

    public MealsAndDrinks getMealsAndDrinks() {
        return mealsAndDrinks;
    }
}
