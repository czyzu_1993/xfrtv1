package orderAndCustomer;

import enumeration.Cuisines;
import mealsAndDrinks.Dessert;
import mealsAndDrinks.Drinks;
import mealsAndDrinks.Meals;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    private List<Drinks> drinkMenu;
    private List<Dessert> dessertMenu;
    private List<Meals> mainMenu;

    public Menu() {
        drinkMenu = new ArrayList<>();
        dessertMenu = new ArrayList<>();
        mainMenu = new ArrayList<>();

        drinkMenu.add(new Drinks(10, "Tee"));
        drinkMenu.add(new Drinks(8, "White coffee"));
        drinkMenu.add(new Drinks(7, "Black Coffee"));
        drinkMenu.add(new Drinks(4, "Water"));
        drinkMenu.add(new Drinks(10, "Orange juice"));
        drinkMenu.add(new Drinks(10, "Apple juice"));

        dessertMenu.add(new Dessert(10, "Cake"));
        dessertMenu.add(new Dessert(10, "Apple"));
        dessertMenu.add(new Dessert(10, "Jelly"));
        dessertMenu.add(new Dessert(10, "Ice creams"));

        mainMenu.add(new Meals(20, "Tomato Soup", Cuisines.POLISH));
        mainMenu.add(new Meals(44, "Lasagne", Cuisines.ITALIAN));
        mainMenu.add(new Meals(42, "Burrito", Cuisines.MEXICAN));
    }

    public List<Dessert> getDessertMenu() {
        for (int i = 0; i < dessertMenu.size(); i++) {
            System.out.println(i + " " + dessertMenu.get(i).getName() + " " + dessertMenu.get(i).getPrice());
        }
        return dessertMenu;
    }

    public List<Drinks> getDrinkMenu() {
        for (int i = 0; i < drinkMenu.size(); i++) {
            System.out.println(i + " " + drinkMenu.get(i).getName() + " " + drinkMenu.get(i).getPrice());
        }
        return drinkMenu;
    }

    public List<Meals> getMainMenu() {
        for (int i = 0; i < mainMenu.size(); i++) {
            System.out.println(i + " " + mainMenu.get(i).getName() + " " + mainMenu.get(i).getCuisies() + " " + mainMenu.get(i).getPrice());
        }
        return mainMenu;
    }
}
