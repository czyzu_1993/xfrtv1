

import orderAndCustomer.Customer;
import orderAndCustomer.Menu;
import mealsAndDrinks.Dessert;
import mealsAndDrinks.Drinks;
import mealsAndDrinks.Meals;


import java.util.List;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        Menu menu = new Menu();
        Customer customer = new Customer();
        int count;
        while (true) {
            System.out.println("ORDERS");
            System.out.println("1. Order Desert");
            System.out.println("2. Order Drinks");
            System.out.println("3. Order Main Course");
            System.out.println("4. Get receipt");
            System.out.println("5. EXIT");
            Scanner in = new Scanner(System.in);
            System.out.println("chose option");
            int i = in.nextInt();

            switch (i) {
                case 1:
                    List<Dessert> dessertList = menu.getDessertMenu();
                    System.out.println("chose Dessert");
                    i = in.nextInt();
                    System.out.println("get count");
                    count = in.nextInt();
                    if (i < dessertList.size()) customer.addOrder(count, dessertList.get(i));
                    else System.out.println("you chose wrong position from list");
                    break;
                case 2:
                    List<Drinks> drinkList = menu.getDrinkMenu();
                    System.out.println("chose Drink");
                    i = in.nextInt();
                    System.out.println("get count");
                    count = in.nextInt();
                    Drinks drinks = new Drinks(drinkList.get(i));

                    System.out.println("1. lemon");
                    System.out.println("2. ice Cubes");
                    System.out.println("3. lemon and ice Cubes");
                    System.out.println("4. nothing");
                    switch (in.nextInt()) {
                        case 1:
                            drinks.setLemon(true);
                            break;
                        case 2:
                            drinks.setIceCubes(true);
                            break;
                        case 3:
                            drinks.setIceCubes(true);
                            drinks.setLemon(true);
                            break;
                        default:
                            System.out.println("you don't chose anything");
                    }
                    drinks.setIceCubes(true);
                    drinks.setLemon(true);
                    if (i < drinkList.size()) customer.addOrder(count, drinkList.get(i));
                    else System.out.println("you chose wrong position from list");
                    break;
                case 3:
                    List<Meals> mainList = menu.getMainMenu();
                    System.out.println("chose main course");
                    i = in.nextInt();
                    System.out.println("get count");
                    count = in.nextInt();
                    if (i < mainList.size()) customer.addOrder(count, mainList.get(i));
                    else System.out.println("you chose wrong position from list");
                    break;
                case 4:
                    System.out.println("you have to pay " + customer.gerReceipt());
                    break;
                case 5:
                    System.exit(0);
                    break;
                default:
                    System.out.println("wrong option");

            }
        }
    }
}
